<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>pushupinc</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="img/logo1.png">

        <link rel="stylesheet" type="text/css" media="screen" href="css/fonts.css">
        <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">

        <style></style>

        @yield('styles')
        
    </head>
    <body>

        <div id="app">
            <!--=== Menu ===-->
            <header>
                <navbar-comp></navbar-comp>
            </header>
            <!--=== end of Menu ===-->
    
            <!--=== Main ===-->
            <main class="width-adj">
                @yield('contents')
            </main>
            <!--=== end of Main ===-->
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="js/main.js"></script>

        @yield('scripts')
    </body>
</html>
