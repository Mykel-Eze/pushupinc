@extends('layouts.main')

@section('style')
    <style>
        .brands-bio{
            padding: 20px;
        }
        .brands-card img{
            width: 100%;
        }
    </style>
@endsection

@section('contents')

<!---=== HOME SECTION ===--->
    <div class="col-xs-12" id="home">
        <div class="col-xs-12 col-sm-5">
            <div class="main-div">
                <h5 class="weare">
                    <span><hr class="line"></span> WE ARE 
                </h5>
                <h1 class="main-header">Pushup Inc</h1>
                <p>
                    PushUp Incorporated is a brand consulting firm, whose sole objective is to
                    create and bring to life ideas using captivating and interactive visuals.
                </p>
            </div>
        </div>

        <div class="col-xs-12 col-sm-7">
            <slider-comp></slider-comp>
        </div>
    </div>
<!---=== end of HOME SECTION ===--->

<!---=== ABOUT SECTION ===--->
    <div class="col-xs-12 pad0" id="about">
        <div class="col-xs-12">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div>
                    <h5 class="weare">
                        <span><hr class="line"></span> WHAT WE DO
                    </h5>
                    <h2 class="header-txt p-bold-font">Captivating and Interactive visuals.</h2>
                    <p>
                        Push up incorporation is a team of proficient, creative, and
                        efficient millennials who are incessantly improving their skills in
                        their field of expertise.
                    </p>
                    <h5 class="weare" style="margin-top: 30px;">
                        Discover more  <span><hr class="animate-h-ln line" style="margin-left: 20px;"></span>
                    </h5>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div>
                    <div class="icon-img text-center">
                        <img src="img/icon-1.png" alt="Icon 1">
                    </div>
                    <div>
                        <h2 class="abt-title p-bold-font">Branding</h2>
                        <p class="abt-txt text-justify">
                            Our branding team infuses creativity with direction by creating a story
                            that will move your brand into a new and exciting phase with high visual
                            content and distinct images.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div>
                    <div class="icon-img text-center">
                        <img src="img/icon-2.png" alt="Icon 2">
                    </div>
                    <div>
                        <h2 class="abt-title p-bold-font">Platforms</h2>
                        <p class="abt-txt text-justify">
                            Your brand story needs to be heard! <br>
                            At pushup incorporation, we develop and design functional, intuitive and
                            aesthetically pleasing platforms that helps tell your story and boost your
                            brand perception and engagement.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div>
                    <div class="icon-img text-center">
                        <img src="img/icon-3.png" alt="Icon 3">
                    </div>
                    <div>
                        <h2 class="abt-title p-bold-font">Strategy</h2>
                        <p class="abt-txt text-justify">
                            We take that brand story out to the world! Through lead generation strategy and
                            measurable metrics, our creative campaign team helps your brand to achieve the right
                            kind of exposure. We help create a lasting impression in the heart of your audience with
                            our creative and strategic campaigns.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!---=== end of ABOUT SECTION ===--->

<!---=== BRANDS SECTION ===--->
    <div class="col-xs-12 pad0" id="brands">
        <div class="col-xs-12">
            <div class="col-xs-12 col-sm-6 pry-bg-color brands-bio">
                <div class="">
                    <h4 class="header-txt">Brands we've worked with</h4>
                    <p>
                        Our customer centric approach has encouraged teaming up with our clients to
                        create world class marques; this has made us one of the best brands to work with.
                    </p>
                </div>
                <div class="brands-card">
                    <div class="col-xs-6 col-sm-4">
                        <a href="#"><img src="img/femfunds.png" alt="Femfunds" style="width: 85%"></a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="#"><img src="img/landlagos.png" alt="LandLagos" style="width: 85%"></a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="#"><img src="img/cmfolorunso.png" alt="cmfolorunso" style="width: 85%"></a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="#"><img src="img/porkoyum.png" alt="Porkoyum" style="width: 85%"></a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="#"><img src="img/smartacad.png" alt="Smartacad" style="width: 85%"></a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="#"><img src="img/proctorer.png" alt="Proctorer" style="width: 85%"></a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="#"><img src="img/porkmoney.png" alt="Porkmoney" style="width: 85%"></a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="#"><img src="img/osei-consult.png" alt="Osei Consult" style="width: 85%"></a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="#"><img src="img/workatdubai.png" alt="Workatdubai" style="width: 85%"></a>
                    </div>
                </div>
            </div>

            <div class="hidden-xs col-sm-6">
                <img src="img/office-watermark.png" alt="office-watermark" style="width: 100%;">
            </div>
        </div>
    </div>
<!---=== end of BRANDS SECTION ===--->

<!---=== CAREERS SECTION ===--->
<div class="col-xs-12 pad0" id="careers">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <div class="career-wrapper">
                <div class="col-xs-12 col-sm-8"></div>

                <div class="col-xs-12 col-sm-4"></div>
            </div>
        </div>
    </div>
<!---=== end of CAREERS SECTION ===--->

@endsection

@section('scripts')
    <script></script>
@endsection
